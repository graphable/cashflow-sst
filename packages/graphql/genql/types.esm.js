export default {
    "scalars": [
        1,
        2,
        3,
        5,
        8
    ],
    "types": {
        "Cashflow": {
            "asOfDate": [
                1
            ],
            "createdAt": [
                1
            ],
            "currentCash": [
                2
            ],
            "entries": [
                4
            ],
            "id": [
                3
            ],
            "status": [
                1
            ],
            "__typename": [
                1
            ]
        },
        "String": {},
        "Float": {},
        "ID": {},
        "CashflowEntry": {
            "dayOfPeriod": [
                5
            ],
            "group": [
                1
            ],
            "id": [
                1
            ],
            "name": [
                1
            ],
            "period": [
                1
            ],
            "value": [
                2
            ],
            "__typename": [
                1
            ]
        },
        "Int": {},
        "Mutation": {
            "createCashflow": [
                0
            ],
            "__typename": [
                1
            ]
        },
        "Query": {
            "cashflow": [
                0,
                {
                    "cashflowId": [
                        1,
                        "String!"
                    ]
                }
            ],
            "cashflows": [
                0
            ],
            "__typename": [
                1
            ]
        },
        "Boolean": {}
    }
}