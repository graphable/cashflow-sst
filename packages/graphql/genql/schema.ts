import {FieldsSelection,Observable} from '@genql/runtime'

export type Scalars = {
    String: string,
    Float: number,
    ID: string,
    Int: number,
    Boolean: boolean,
}

export interface Cashflow {
    asOfDate: Scalars['String']
    createdAt: Scalars['String']
    currentCash: Scalars['Float']
    entries: CashflowEntry[]
    id: Scalars['ID']
    status: Scalars['String']
    __typename: 'Cashflow'
}


/** An entry in the cashflow item */
export interface CashflowEntry {
    dayOfPeriod: Scalars['Int']
    group: Scalars['String']
    id: Scalars['String']
    name: Scalars['String']
    period: Scalars['String']
    value: Scalars['Float']
    __typename: 'CashflowEntry'
}

export interface Mutation {
    createCashflow: Cashflow
    __typename: 'Mutation'
}

export interface Query {
    cashflow: Cashflow
    cashflows: Cashflow[]
    __typename: 'Query'
}

export interface CashflowRequest{
    asOfDate?: boolean | number
    createdAt?: boolean | number
    currentCash?: boolean | number
    entries?: CashflowEntryRequest
    id?: boolean | number
    status?: boolean | number
    __typename?: boolean | number
    __scalar?: boolean | number
}


/** An entry in the cashflow item */
export interface CashflowEntryRequest{
    dayOfPeriod?: boolean | number
    group?: boolean | number
    id?: boolean | number
    name?: boolean | number
    period?: boolean | number
    value?: boolean | number
    __typename?: boolean | number
    __scalar?: boolean | number
}

export interface MutationRequest{
    createCashflow?: CashflowRequest
    __typename?: boolean | number
    __scalar?: boolean | number
}

export interface QueryRequest{
    cashflow?: [{cashflowId: Scalars['String']},CashflowRequest]
    cashflows?: CashflowRequest
    __typename?: boolean | number
    __scalar?: boolean | number
}


const Cashflow_possibleTypes: string[] = ['Cashflow']
export const isCashflow = (obj?: { __typename?: any } | null): obj is Cashflow => {
  if (!obj?.__typename) throw new Error('__typename is missing in "isCashflow"')
  return Cashflow_possibleTypes.includes(obj.__typename)
}



const CashflowEntry_possibleTypes: string[] = ['CashflowEntry']
export const isCashflowEntry = (obj?: { __typename?: any } | null): obj is CashflowEntry => {
  if (!obj?.__typename) throw new Error('__typename is missing in "isCashflowEntry"')
  return CashflowEntry_possibleTypes.includes(obj.__typename)
}



const Mutation_possibleTypes: string[] = ['Mutation']
export const isMutation = (obj?: { __typename?: any } | null): obj is Mutation => {
  if (!obj?.__typename) throw new Error('__typename is missing in "isMutation"')
  return Mutation_possibleTypes.includes(obj.__typename)
}



const Query_possibleTypes: string[] = ['Query']
export const isQuery = (obj?: { __typename?: any } | null): obj is Query => {
  if (!obj?.__typename) throw new Error('__typename is missing in "isQuery"')
  return Query_possibleTypes.includes(obj.__typename)
}


export interface CashflowPromiseChain{
    asOfDate: ({get: (request?: boolean|number, defaultValue?: Scalars['String']) => Promise<Scalars['String']>}),
    createdAt: ({get: (request?: boolean|number, defaultValue?: Scalars['String']) => Promise<Scalars['String']>}),
    currentCash: ({get: (request?: boolean|number, defaultValue?: Scalars['Float']) => Promise<Scalars['Float']>}),
    entries: ({get: <R extends CashflowEntryRequest>(request: R, defaultValue?: FieldsSelection<CashflowEntry, R>[]) => Promise<FieldsSelection<CashflowEntry, R>[]>}),
    id: ({get: (request?: boolean|number, defaultValue?: Scalars['ID']) => Promise<Scalars['ID']>}),
    status: ({get: (request?: boolean|number, defaultValue?: Scalars['String']) => Promise<Scalars['String']>})
}

export interface CashflowObservableChain{
    asOfDate: ({get: (request?: boolean|number, defaultValue?: Scalars['String']) => Observable<Scalars['String']>}),
    createdAt: ({get: (request?: boolean|number, defaultValue?: Scalars['String']) => Observable<Scalars['String']>}),
    currentCash: ({get: (request?: boolean|number, defaultValue?: Scalars['Float']) => Observable<Scalars['Float']>}),
    entries: ({get: <R extends CashflowEntryRequest>(request: R, defaultValue?: FieldsSelection<CashflowEntry, R>[]) => Observable<FieldsSelection<CashflowEntry, R>[]>}),
    id: ({get: (request?: boolean|number, defaultValue?: Scalars['ID']) => Observable<Scalars['ID']>}),
    status: ({get: (request?: boolean|number, defaultValue?: Scalars['String']) => Observable<Scalars['String']>})
}


/** An entry in the cashflow item */
export interface CashflowEntryPromiseChain{
    dayOfPeriod: ({get: (request?: boolean|number, defaultValue?: Scalars['Int']) => Promise<Scalars['Int']>}),
    group: ({get: (request?: boolean|number, defaultValue?: Scalars['String']) => Promise<Scalars['String']>}),
    id: ({get: (request?: boolean|number, defaultValue?: Scalars['String']) => Promise<Scalars['String']>}),
    name: ({get: (request?: boolean|number, defaultValue?: Scalars['String']) => Promise<Scalars['String']>}),
    period: ({get: (request?: boolean|number, defaultValue?: Scalars['String']) => Promise<Scalars['String']>}),
    value: ({get: (request?: boolean|number, defaultValue?: Scalars['Float']) => Promise<Scalars['Float']>})
}


/** An entry in the cashflow item */
export interface CashflowEntryObservableChain{
    dayOfPeriod: ({get: (request?: boolean|number, defaultValue?: Scalars['Int']) => Observable<Scalars['Int']>}),
    group: ({get: (request?: boolean|number, defaultValue?: Scalars['String']) => Observable<Scalars['String']>}),
    id: ({get: (request?: boolean|number, defaultValue?: Scalars['String']) => Observable<Scalars['String']>}),
    name: ({get: (request?: boolean|number, defaultValue?: Scalars['String']) => Observable<Scalars['String']>}),
    period: ({get: (request?: boolean|number, defaultValue?: Scalars['String']) => Observable<Scalars['String']>}),
    value: ({get: (request?: boolean|number, defaultValue?: Scalars['Float']) => Observable<Scalars['Float']>})
}

export interface MutationPromiseChain{
    createCashflow: (CashflowPromiseChain & {get: <R extends CashflowRequest>(request: R, defaultValue?: FieldsSelection<Cashflow, R>) => Promise<FieldsSelection<Cashflow, R>>})
}

export interface MutationObservableChain{
    createCashflow: (CashflowObservableChain & {get: <R extends CashflowRequest>(request: R, defaultValue?: FieldsSelection<Cashflow, R>) => Observable<FieldsSelection<Cashflow, R>>})
}

export interface QueryPromiseChain{
    cashflow: ((args: {cashflowId: Scalars['String']}) => CashflowPromiseChain & {get: <R extends CashflowRequest>(request: R, defaultValue?: FieldsSelection<Cashflow, R>) => Promise<FieldsSelection<Cashflow, R>>}),
    cashflows: ({get: <R extends CashflowRequest>(request: R, defaultValue?: FieldsSelection<Cashflow, R>[]) => Promise<FieldsSelection<Cashflow, R>[]>})
}

export interface QueryObservableChain{
    cashflow: ((args: {cashflowId: Scalars['String']}) => CashflowObservableChain & {get: <R extends CashflowRequest>(request: R, defaultValue?: FieldsSelection<Cashflow, R>) => Observable<FieldsSelection<Cashflow, R>>}),
    cashflows: ({get: <R extends CashflowRequest>(request: R, defaultValue?: FieldsSelection<Cashflow, R>[]) => Observable<FieldsSelection<Cashflow, R>[]>})
}