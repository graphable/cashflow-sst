
var Cashflow_possibleTypes = ['Cashflow']
export var isCashflow = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCashflow"')
  return Cashflow_possibleTypes.includes(obj.__typename)
}



var CashflowEntry_possibleTypes = ['CashflowEntry']
export var isCashflowEntry = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCashflowEntry"')
  return CashflowEntry_possibleTypes.includes(obj.__typename)
}



var Mutation_possibleTypes = ['Mutation']
export var isMutation = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isMutation"')
  return Mutation_possibleTypes.includes(obj.__typename)
}



var Query_possibleTypes = ['Query']
export var isQuery = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isQuery"')
  return Query_possibleTypes.includes(obj.__typename)
}
