import { Cashflow } from "../../../../core/cashflow";
import { builder } from "../builder";

export class CashflowEntryType {
  id: string;
  name: string;
  group: string;
  value: number;
  period: "one-time" | "week" | "month" | "quarter" | "year";
  dayOfPeriod: number;

  constructor(
    id: string,
    name: string,
    group: string,
    value: number,
    period: "one-time" | "week" | "month" | "quarter" | "year",
    dayOfPeriod: number
  ) {
    this.id = id;
    this.name = name;
    this.group = group;
    this.value = value;
    this.period = period;
    this.dayOfPeriod = dayOfPeriod;
  }
}

builder.objectType(CashflowEntryType, {
  name: "CashflowEntry",
  description: "An entry in the cashflow item",
  fields: (t) => ({
    id: t.exposeString("id"),
    name: t.exposeString("name"),
    group: t.exposeString("group"),
    value: t.exposeFloat("value"),
    period: t.exposeString("period"),
    dayOfPeriod: t.exposeInt("dayOfPeriod"),
  }),
});

const CashflowType = builder
  .objectRef<Cashflow.CashflowEntityType>("Cashflow")
  .implement({
    fields: (t) => ({
      id: t.exposeID("cashflowId"),
      createdAt: t.exposeString("createdAt"),
      asOfDate: t.exposeString("asOfDate"),
      status: t.exposeString("status"),
      currentCash: t.exposeFloat("currentCash"),
      entries: t.field({
        type: [CashflowEntryType],
        resolve: (parent) => parent.entries,
      }),
    }),
  });

builder.queryFields((t) => ({
  cashflow: t.field({
    type: CashflowType,
    args: {
      cashflowId: t.arg.string({ required: true }),
    },
    resolve: async (_, args) => {
      const result = await Cashflow.get(args.cashflowId);

      if (!result) {
        throw new Error("Article not found");
      }

      return result;
    },
  }),
  cashflows: t.field({
    type: [CashflowType],
    resolve: () => Cashflow.list(),
  }),
}));

builder.mutationFields((t) => ({
  createCashflow: t.field({
    type: CashflowType,
    args: {},
    resolve: (_, args) => Cashflow.create(),
  }),
  /*updateCurrentCash: t.field({
    type: CashflowType,
    args: {
      cashflowId: t.arg({
        type: "String",
        required: true,
        description: "CashflowID to update",
      }),
      currentCash: t.arg({
        type: "Int",
        required: true,
        description: "current cash int",
      }),
    },
    resolve: (_, args) =>
      Cashflow.updateCurrentCash(args.cashflowId, args.currentCash),
  }),*/
}));
