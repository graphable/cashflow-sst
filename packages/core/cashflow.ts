import { ulid } from "ulid";
import { Entity, EntityItem } from "electrodb";
import { Dynamo } from "./dynamo";
import { DateTime } from "luxon";
import { CashflowEntryType } from "../functions/src/graphql/types/cashflow";

export * as Cashflow from "./cashflow";

//TODO/notes: One-time edits of recurring payments, Slowly changing dimensions on recurring payments. Notes on value updates
//TODO pull in CC balance and LOC balance, currentShortTermDebt balance, totalAvailableShortTermDebt
//Chart for trend of projected balance, maybe months of cash as well
//color scheme for balance would be white/green for positive, yellow for positive but short term debt, red for negative

export const CashflowEntity = new Entity(
  {
    model: {
      version: "1",
      entity: "Cashflow",
      service: "cashflow",
    },
    attributes: {
      cashflowId: {
        type: "string",
        required: true,
        readOnly: true,
      },
      createdAt: {
        type: "string",
        required: true,
        readOnly: true,
      },
      asOfDate: {
        type: "string",
        required: true,
      },
      currentCash: {
        type: "number",
        required: true,
      },
      status: {
        type: ["active", "deleted"] as const,
        required: true,
      },
      entries: {
        type: "list",
        required: true,
        items: {
          type: "map",
          properties: {
            id: {
              type: "string",
              readOnly: true,
              required: true,
            },
            name: {
              type: "string",
              required: true,
            },
            group: {
              type: "string",
              required: true,
            },
            value: {
              type: "number",
              required: true,
            },
            period: {
              type: ["one-time", "week", "month", "quarter", "year"] as const,
              required: true,
            },
            dayOfPeriod: {
              type: "number",
              required: true,
            },
          },
        },
      },
    },
    indexes: {
      primary: {
        pk: {
          field: "pk",
          composite: [],
        },
        sk: {
          field: "sk",
          composite: ["cashflowId"],
        },
      },
    },
  },
  Dynamo.Configuration
);

export type CashflowEntityType = EntityItem<typeof CashflowEntity>;

export async function create() {
  const result = await CashflowEntity.create({
    cashflowId: ulid(),
    createdAt: DateTime.now().toISO(),
    asOfDate: DateTime.now().toFormat("yyyy-MM-dd"),
    status: "active",
    currentCash: 0,
    entries: [],
  }).go();

  return result.data;
}

export async function updateCurrentCash(
  cashflowId: string,
  currentCash: number
) {
  const result = await CashflowEntity.patch({ cashflowId: cashflowId }).set({
    currentCash: currentCash,
  });

  return result.data;
}

export async function addCashflowEntry(
  cashflowId: string,
  entry: CashflowEntryType
) {
  const result = await CashflowEntity.patch({ cashflowId: cashflowId }).append({
    entries: [entry],
  });

  return result.data;
}

export async function get(cashflowId: string) {
  const result = await CashflowEntity.get({ cashflowId }).go();

  return result.data;
}

export async function list() {
  const result = await CashflowEntity.query.primary({}).go();

  return result.data;
}
