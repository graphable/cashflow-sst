import { SSTConfig } from "sst";
import { Database } from "./stacks/database";
import { Web } from "./stacks/web";
import { Api } from "./stacks/api";

export default {
  config(_input) {
    return {
      name: "cashflow",
      region: "us-east-1",
    };
  },
  stacks(app) {
    app.stack(Database);
    app.stack(Api);
    app.stack(Web);
  },
} satisfies SSTConfig;
