import { Grid, Tab, Tabs } from "@mui/material";
import { Link, useLocation, Location } from "react-router-dom";

interface LeftNavProps {
  options: {
    label: string;
    path: string;
  }[];
}

const LeftNav = ({ options }: LeftNavProps) => {
  let location: Location = useLocation();
  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Tabs
          variant={"scrollable"}
          orientation={"vertical"}
          value={options.findIndex((option) =>
            location.pathname.includes(option.path)
          )}
        >
          {options.map((option) => {
            return option.label !== undefined ? (
              <Tab
                key={option.path}
                label={option.label}
                component={Link}
                to={`${option.path}`}
                sx={{
                  fontWeight: 500,
                }}
              />
            ) : null;
          })}
        </Tabs>
      </Grid>
    </Grid>
  );
};

export default LeftNav;
