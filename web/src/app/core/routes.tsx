import { useState } from "react";
import reactLogo from "./assets/react.svg";
import { Navigate, Route, Routes } from "react-router-dom";
import Layout from "./layout.component";
import Cashflow from "../features/cashflow";

function App() {
  const [count, setCount] = useState(0);

  return (
    <Routes>
      <Route path={"/"} element={<Layout />}>
        <Route index element={<Navigate to="cashflow" />} />
        <Route path="cashflow/*" element={<Cashflow />} />
        <Route
          path="*"
          element={
            <main style={{ padding: "1rem" }}>
              <p>There's nothing here!</p>
            </main>
          }
        />
      </Route>
    </Routes>
  );
}

export default App;
