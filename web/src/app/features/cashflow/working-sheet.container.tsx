import Grid from "@mui/material/Unstable_Grid2";
import { Box, TextField, Typography } from "@mui/material";
import { DatePicker } from "@mui/x-date-pickers";
import { useState } from "react";
import { Stack } from "@mui/material";
import { DataGrid, GridColDef } from "@mui/x-data-grid";
import { DateTime } from "luxon";

interface WorkingSheetState {
  asOfDate: string | null;
  entries: any[];
  totalRecurringCosts: number;
  currentCash: number;
}

interface Entry {
  id: string;
  name: string;
  type: string;
  typeGroup: "ONE_TIME" | "RECURRING";
  date: string;
  value: number;
}

interface RecurringCost {
  id: string;
  name: string;
  type: string;
  typeGroup: "RECURRING";
  dayOfMonth: number;
  value: number;
}

interface CashflowItem extends Entry {
  balance: number;
  monthsOfCash: string;
}

const columns: GridColDef[] = [
  {
    field: "name",
    headerName: "Entry",
    headerAlign: "left",
    flex: 1.4,
    align: "left",
  },
  {
    field: "type",
    headerName: "Entry Type",
    headerAlign: "center",
    flex: 1.4,
    align: "center",
  },
  {
    field: "date",
    headerName: "Entry Date",
    headerAlign: "center",
    flex: 1.4,
    align: "center",
    type: "date",
  },
  {
    field: "value",
    headerName: "Amount",
    headerAlign: "right",
    flex: 1.4,
    align: "right",
    type: "number",
  },
  {
    field: "balance",
    headerName: "Balance",
    headerAlign: "right",
    flex: 1.4,
    align: "right",
    type: "number",
  },
  {
    field: "monthsOfCash",
    headerName: "Months of Cash",
    headerAlign: "center",
    flex: 1.4,
    align: "center",
  },
];

const entries: Entry[] = [
  {
    id: "12345",
    name: "Test Deduction",
    value: -200,
    type: "PAYMENT",
    typeGroup: "ONE_TIME",
    date: "2023-02-11",
  },
  {
    id: "123434565",
    name: "Test Payment",
    value: 1000000,
    type: "PAYMENT",
    typeGroup: "ONE_TIME",
    date: "2023-02-01",
  },
  {
    id: "123452345",
    name: "Test Deduction",
    value: -200,
    type: "PAYMENT",
    typeGroup: "ONE_TIME",
    date: "2023-02-09",
  },
  {
    id: "1234435yergh5",
    name: "Test Deduction",
    value: -200,
    type: "PAYMENT",
    typeGroup: "ONE_TIME",
    date: "2023-02-07",
  },
];
//TODO and recurring frequency: MONTHLY, QUARTERLY, ANNUALLY
const recurringCosts: RecurringCost[] = [
  {
    id: "1234545",
    name: "Payroll 1",
    value: -123000,
    type: "PAYROLL",
    typeGroup: "RECURRING",
    dayOfMonth: 12,
  },
  {
    id: "1234545",
    name: "Payroll 2",
    value: -90000,
    type: "PAYROLL",
    typeGroup: "RECURRING",
    dayOfMonth: 28,
  },
  {
    id: "1234545345634",
    name: "Monthly Miscellaneous",
    value: -10000,
    type: "MISC",
    typeGroup: "RECURRING",
    dayOfMonth: 28,
  },
];

interface ReduceTypes {
  sum: number;
  res: CashflowItem[];
}

function getRecurringCostEntries(array: RecurringCost[]): Entry[] {
  let currentMonth = DateTime.now().month;
  let currentYear = DateTime.now().year;
  let months = [0, 1, 2, 3, 4, 5, 6];
  let resultEntries: Entry[] = [];
  months.forEach((month) => {
    array.forEach((cost) => {
      let entryDate = DateTime.fromFormat(
        `${currentYear}-${month + currentMonth}-${cost.dayOfMonth}`,
        "yyyy-L-d"
      );
      resultEntries.push({
        id: `${entryDate.toISO()}.${cost.id}`,
        value: cost.value,
        name: `${entryDate.toFormat("LLL")} - ${cost.name}`,
        type: cost.type,
        typeGroup: "RECURRING",
        date: entryDate.toFormat("yyyy-LL-dd"),
      });
    });
  });
  return resultEntries;
}

function getCashflowItems(
  array: Entry[],
  cash: number,
  monthlyCosts: number
): CashflowItem[] {
  const result = array
    .sort((a: Entry, b: Entry) =>
      DateTime.fromISO(a.date)
        .diff(DateTime.fromISO(b.date), "hours")
        .toMillis()
    )
    .reduce(
      (acc, val) => {
        let { sum, res } = acc;
        sum += val.value;
        res.push({
          ...val,
          balance: sum,
          monthsOfCash: (sum / (-1 * monthlyCosts)).toFixed(1),
        });
        return { sum, res };
      },
      { sum: cash, res: [] } as ReduceTypes
    );
  return result.res;
}

function WorkingSheet() {
  const [state, setState] = useState<WorkingSheetState>({
    asOfDate: "2023-02-07",
    entries: entries.concat(getRecurringCostEntries(recurringCosts)),
    totalRecurringCosts: recurringCosts.reduce(
      (acc, val) => acc + val.value,
      0
    ),
    currentCash: 0,
  });
  return (
    <Grid container>
      <Grid xs={8}>
        <Stack direction={"row"}>
          <Typography variant={"h6"}>Current Cash in Bank</Typography>
          <Box maxWidth={200}>
            <TextField
              label={"Cash"}
              value={state.currentCash}
              onChange={(event) =>
                setState((current) => ({
                  ...current,
                  currentCash: parseInt(event.target.value ?? 0),
                }))
              }
            />
          </Box>
          <Typography variant={"h6"}> as of </Typography>
          <Box maxWidth={200}>
            <DatePicker
              label={"Date"}
              value={state.asOfDate}
              onChange={(value) =>
                setState((current) => ({ ...current, asOfDate: value }))
              }
              renderInput={(params) => <TextField {...params} />}
            />
          </Box>
        </Stack>
      </Grid>
      <Grid xs={4} textAlign={"right"}>
        <Typography variant={"body1"}>
          {state.totalRecurringCosts} monthly costs
        </Typography>
      </Grid>
      <Grid mt={2} xs={12}>
        <Box height={600} width={"100%"}>
          <DataGrid
            columns={columns}
            rows={getCashflowItems(
              state.entries,
              state.currentCash,
              state.totalRecurringCosts
            )}
          />
        </Box>
      </Grid>
    </Grid>
  );
}

export default WorkingSheet;
