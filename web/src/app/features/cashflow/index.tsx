import { Grid } from "@mui/material";
import LeftNav from "../../core/leftnav.component";
import { Navigate, Outlet, Route, Routes } from "react-router-dom";
import WorkingSheet from "./working-sheet.container";

const options = [
  {
    label: "Working Sheet",
    path: "working_sheet",
    element: <WorkingSheet />,
  },
  {
    label: "Entries",
    path: "entries",
    element: "Entries",
  },
  {
    label: "Recurring Costs",
    path: "recurring_costs",
    element: "Recurring costs",
  },
];

const Cashflow = () => {
  return (
    <Routes>
      <Route path="" element={<CashflowLayout />}>
        <Route index element={<Navigate to={options[0].path} />} />
        {options.map((option) => (
          <Route
            key={option.path}
            path={option.path}
            element={option.element}
          />
        ))}
      </Route>
    </Routes>
  );
};
const CashflowLayout = () => {
  return (
    <Grid container spacing={2} mt={0.5}>
      <Grid item xs={1}>
        <LeftNav options={options} />
      </Grid>
      <Grid item xs>
        <Outlet />
      </Grid>
    </Grid>
  );
};

export default Cashflow;
