import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter as Router } from "react-router-dom";
import {
  CssBaseline,
  ThemeOptions,
  ThemeProvider,
  createTheme,
  PaletteColor,
} from "@mui/material";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterLuxon } from '@mui/x-date-pickers/AdapterLuxon';
import Routes from "./app/core/routes";

let theme: ThemeOptions;
theme = createTheme({
  palette: {
    background: { default: "#FFFFFF" },
    common: {
      black: "#001021",
    },
    primary: {
      main: "#0caadc",
    },
    secondary: {
      main: "#E9E6FF",
    },
    success: {
      main: "#419d78",
    },
    error: {
      main: "#b3001b",
    },
  },
  typography: {
    fontFamily: "Montserrat, Helvetica, sans-serif",
  },
  components: {
    MuiButtonBase: {
      defaultProps: {
        disableRipple: true,
      },
      styleOverrides: {
        root: {
          textTransform: "none",
          borderRadius: 28,
        },
      },
    },
    MuiTextField: {
      defaultProps: {
        size: "small",
        fullWidth: true,
      },
    },
  },
});

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <Router>
      <LocalizationProvider dateAdapter={AdapterLuxon}>
        <ThemeProvider theme={theme}>
          <CssBaseline />

          <Routes />
        </ThemeProvider>
      </LocalizationProvider>
    </Router>
  </React.StrictMode>
);
